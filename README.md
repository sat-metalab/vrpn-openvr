# VRPN-OpenVR
OpenVR support for VRPN. Builds as a server that exposes OpenVR HMDs and controllers as as openvr/[hmd|controller]/[trackedDeviceId].
Each of which having the relevant vrpn devices that it supports: vrpn_Tracker for HMDs and vrpn_Tracker, crpn_Analog and vrpn_Button
for the controllers.

# Installing dependencies
Following the [README](https://github.com/ValveSoftware/SteamVR-for-Linux) from SteamVR for Linux to install Steam, SteamVR and Nvidia drivers, the installation procedure is as follow:

## On Linux
Install Steam from [STEAM](https://store.steampowered.com/about/)
And SteamVR from the [Steam Store](https://store.steampowered.com/app/250820/SteamVR/)

You need the latest nvidia drivers (at least 430.26) to be able to run SteamVR on Linux:

      sudo add-apt-repository ppa:graphics-drivers/ppa
      sudo apt update
      sudo apt install nvidia-430  # or higher if available

## On Windows
Run the downloaded executable to install Steam.

## Post installation

Install SteamVR directly from Steam and run the room setup application.


# Building
This server builds in Visual Studio (2015 was used) and Linux.
We ran into issues trying to build it with CMake under Cygwin. A CMake file generating a VS project was considered
but was found to introduce too many steps for the end result still requiring switching to VS for the build.

* Initialize the repository with submodules in order to retrieve OpenVR and VRPN

      git submodule update --init --recursive

## On Linux:
* Create a build directory at the root of the project and set the current directory to it:

      mkdir build && cd build

* Generate cmake files and build it:

      cmake .. && make -j$(nproc)

## On Windows:
Open the `VRNP-OpenVR.sln` solution and build from the IDE


# Running

## On Linux

Run your vrpnOpenVR binary in the Steam runtime, from the build directory (if you are elsewhere just change the path
of the binary):

    ~/.steam/steam/ubuntu12_32/steam-runtime/run.sh ./vrpnOpenVR

## On Windows
Just launch the program from the VS solution (F5 to simple run or Ctrl+F5 for debug). Otherwise just click the play
button at the top of the window.


# Troubleshooting

## Headset is not detected properly

A few ideas to fix this:

* activate / deactivate the direct mode
* unplug / replug the lighthouses
* reboot the computer
* check that your graphic driver is up-to-date: SteamVR needs the very latest drivers which are not always packaged in the default repositories, nor in the Graphic Drivers PPA.

# Acknowledgements
This is still very much a work in progress and is by no means a complete and stable solution for using OpenVR with VRPN.
The primary goal of this server is to provide controller data to any VRPN client.


## Notes

To allow using SteamVR without a headset, for example to use only the VR controllers, you have to enable the NULL driver. Edit the file located in `${HOME/.local/share/Steam/config/steamvr.vrsettings` and replace its content with the following:

```json
{
   "jsonid" : "vrsettings",
   "steamvr" : {
      "directModeEdidPid" : 43521,
      "directModeEdidVid" : 53794,
      "forcedDriver" : "null",
      "requireHmd" : true,
      "enableHomeApp" : false,
      "activateMultipleDrivers" : true
   },
   "driver_null" : {
      "enable" : true,
      "id" : "Null Driver",
      "serialNumber" : "Null 4711",
      "modelNumber" : "Null Model Number",
      "windowX" : 100,
      "windowY" : 100,
      "windowWidth" : 1920,
      "windowHeight" : 1080,
      "renderWidth" : 1280,
      "renderHeight" : 720,
      "secondsFromVsyncToPhotons" : 0.1,
      "displayFrequency" : 60
   }
}
```

You may have to restart SteamVR multiple times for the configuration to be applied correctly.
